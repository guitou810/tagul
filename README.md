## Description
Ce projet permet de convertir des nombres arabes en un nombre Romain.
## Statut
KO
## Installation
clone projet
```
$ git clone [URL]
```
en premier installer node.js
```
$ npm init
```
ensuite installer Jest
```
$ npm install jest
```
## How to
## Documentation
README.md
## Author
## Dependencies
## Support
## Insperation


![Hackerman](https://pbs.twimg.com/profile_images/1035079978008948737/NNtdoxpw_400x400.jpg)

## Description

This project aims to get the roman number corresponding to a given number.

### exemple:
````bash
3 -> III
4 -> IV
````


## Structure

-   `src/app`
-   `config`
-   `bin`

## Installation

1. Clone the repository.

```shell
$ git clone https://gitlab.com/<username>/roman.git
```

2. Install dependencies.

```shell
$ cd roman
$ npm install
```

3. Install vscode extensions.
   
Install the recommended extensions via the extension tab.

4. Build and run the project.

Double-clic on index.js

## UML diagrams

### Exemple : Convertir un chiffre en chiffre romain (3 => III)

```mermaid
graph LR
A[Début] -- chiffre = 3  --> B{chiffre < 4 }
B -- chiffreRomain --> C[for =0 i < chffre i++]
C -- chiffreRomain += I  --> C
C -- chiffreRomaine = III --> D[Fin]
```

## Auteurs
Creator: Jalil Arfaoui
This project is developped by [Stephen SOUPART](https://github.com/Stephen81s), [Clément MOLINIE](https://github.com/ClementM0512) and the students of ESN81.
